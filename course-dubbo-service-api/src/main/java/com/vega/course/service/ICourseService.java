package com.vega.course.service;

import com.vega.course.dto.CourseDTO;

import java.util.List;

/**
 * 功能描述: <br>
 * 所属包名: com.vega.course.service<br>
 * 创建人　: 白剑<br>
 * 创建时间: 05/11/2018 07:18 Friday<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public interface ICourseService {

    List<CourseDTO> courseList();
}
