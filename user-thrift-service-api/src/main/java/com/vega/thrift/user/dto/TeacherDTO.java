package com.vega.thrift.user.dto;

/**
 * 功能描述: <br>
 * 所属包名: com.vega.thrift.user.dto<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/05/11 10:12:48<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public class TeacherDTO extends UserDTO {

    private String intro;
    private int stars;

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }
}
