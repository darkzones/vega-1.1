package com.vega.message.service;

import com.vega.thrift.message.MessageService;
import org.apache.thrift.TException;
import org.springframework.stereotype.Service;

/**
 * 功能描述: <br>
 * 所属包名: com.vega.message.service<br>
 * 创建人　: 白剑<br>
 * 创建时间: 05/07/2018 00:17 Monday<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
@Service
public class MessageServiceImpl implements MessageService.Iface {

    @Override
    public boolean sendMobileMessage(String mobile, String message) throws TException {
        System.out.println("------- start -------");
        System.out.println(String.format("mobile: %s, message: %s", mobile, message));
        System.out.println("------- end -------");
        return true;
    }

    @Override
    public boolean sendEmailMessage(String email, String message) throws TException {
        System.out.println("------- start -------");
        System.out.println(String.format("email: %s, message: %s", email, message));
        System.out.println("------- end -------");
        return true;
    }
}
