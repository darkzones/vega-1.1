package com.vega.message.thrift;

import com.vega.thrift.message.MessageService;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

/**
 * 功能描述: <br>
 * 所属包名: com.vega.message.thrift<br>
 * 创建人　: 白剑<br>
 * 创建时间: 05/07/2018 00:28 Monday<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public class ThriftServerTest {

    public static final int SERVER_PORT = 7811;
    public static final String SERVER_IP = "127.0.0.1";

    public void startClient(String username) {

        try (TTransport tTransport = new TFramedTransport(new TSocket(SERVER_IP, SERVER_PORT))) {

            TProtocol protocol = new TBinaryProtocol(tTransport);
            MessageService.Client client = new MessageService.Client(protocol);
            tTransport.open();

            boolean result = client.sendEmailMessage("572741916@qq.com", "hello thrift");
            System.out.println("Thrift client result = " + result);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ThriftServerTest test = new ThriftServerTest();
        test.startClient("zfy");
    }
}
