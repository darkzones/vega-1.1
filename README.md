# vega-1.1

基于spring boot/spring cloud构建的微服务容器化项目，包含单点登录、redis缓存服务器、dubbo/thrift、api网关、zk、docker等

主要是为了尝试微服务容器化下的开发模式，以后还会加入k8s服务编排、CICD、DevOps等。