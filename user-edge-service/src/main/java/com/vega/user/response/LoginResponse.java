package com.vega.user.response;

/**
 * 功能描述: <br>
 * 所属包名: com.vega.user.response<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/05/07 22:46:38<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public class LoginResponse extends Response {

    private String token;

    public LoginResponse(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
