package com.vega.user.response;

import java.io.Serializable;

/**
 * 功能描述: <br>
 * 所属包名: com.vega.user.response<br>
 * 创建人　: 白剑<br>
 * 创建时间: 05/07/2018 21:26 Monday<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
public class Response implements Serializable {

    public static final Response USERNAME_PASSWORD_INVALID = new Response("1001", "用户名或密码错误");
    public static final Response MOBILE_OR_EMAIL_REQUIRED = new Response("1002", "手机或邮箱必填");
    public static final Response SEND_VERIFYCODE_FAILED = new Response("1003", "验证码发送失败");
    public static final Response VERIFYCODE_INVALID = new Response("1004", "验证码不正确");
    public static final Response SUCCESS = new Response();

    private String code;
    private String message;

    public Response() {
        this.code = "0000";
        this.message = "success";
    }

    public Response(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public static Response exception(Exception e) {
        return new Response("9999", e.getMessage());
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
