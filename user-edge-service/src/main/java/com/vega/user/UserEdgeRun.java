package com.vega.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 功能描述: <br>
 * 所属包名: com.vega.user<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/05/06 23:58:15<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
@SpringBootApplication
public class UserEdgeRun {

    public static void main(String[] args) {
        SpringApplication.run(UserEdgeRun.class, args);
    }
}
