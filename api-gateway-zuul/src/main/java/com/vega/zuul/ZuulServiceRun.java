package com.vega.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * 功能描述: <br>
 * 所属包名: com.vega.zuul<br>
 * 创建人　: 白剑<br>
 * 创建时间: 2018/05/11 11:29:48<br>
 * 当前版本: 1.0<br>
 * 修改历史: <br>
 * 修改时间　　　　　修改人　　　　　版本变更　　　　　修改说明<br>
 * -----------------------------------------------------<br>
 * <br>
 */
@SpringBootApplication
@EnableZuulProxy
public class ZuulServiceRun {

    public static void main(String[] args) {
        SpringApplication.run(ZuulServiceRun.class, args);
    }
}
